<?php
ob_start();
session_start();
?>
<?php 
include('connection.php');
include('library/php_qr_code/qrlib.php'); // Include a library for PHP QR code

if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
	
	
	// echo "<pre>";
	// print_r($_POST);
	// print_r($_FILES);die;
	$total = count($_FILES['myfile']['name']);
	// echo $total;
	//die;
	if($total>0)
	{
		$added_date=date("Y-m-d H:i:s");
		$upload_by=$_SESSION['sess_user_id'];
		$file_id = uniqid(); 
		// Loop through each file
		for( $i=0 ; $i < $total ; $i++ ) {

		  //Get the temp file path
		  $tmpFilePath = $_FILES['myfile']['tmp_name'][$i];

		  //Make sure we have a file path
		  if ($tmpFilePath != ""){
		    //Setup our new file path
		    $user_file = $_FILES['myfile']['name'][$i];
		    $ext = pathinfo($user_file, PATHINFO_EXTENSION);
		    $uniquesavename=time().uniqid(rand());
		    $newfname = $uniquesavename.'.'.$ext;
		    $newFilePath = "./uploadFiles/" . $newfname;

		    //Upload the file into the temp dir
		    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
			   $title=($_POST['titles'][$i])?$_POST['titles'][$i]:'';
		      $sql = "INSERT INTO `tbl_files`(`file_id`,`title`,`filename`,`user_file`,`added_date`, `added_by`) VALUES ('$file_id','$title','$newFilePath','$user_file','$added_date','$upload_by')";
				if (mysqli_query($conn, $sql)) {
					// echo'Inserted';
				}
				else {
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
		    }
		  }
		}


		//its a location where generated QR code can be stored.
		$qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
		$set_qr_code_path = 'qr_assets/';

		// If directory is not created, the create a new directory 
		if(!file_exists($qr_code_file_path)){
	    	mkdir($qr_code_file_path);
		}
		
		//Set a file name of each generated QR code
		$time=time();
		$filename	=	$qr_code_file_path.$time.'.png';
	}

	// die('file uploaded');
	//its a location where generated QR code can be stored.
	$qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
	$set_qr_code_path = 'qr_assets/';

	// If directory is not created, the create a new directory 
	if(!file_exists($qr_code_file_path)){
    	mkdir($qr_code_file_path);
	}
	
	//Set a file name of each generated QR code
	$time=time();
	$filename	=	$qr_code_file_path.$time.'.png';
	
	/* All the user generated data must be sanitize before the processing */
	 if (isset($_REQUEST['level']) && $_REQUEST['level']!=''){
	 	$errorCorrectionLevel = $_REQUEST['level'];
	 }else{
	 	$errorCorrectionLevel = 'H';
	 }
    

	 if (isset($_REQUEST['size']) && $_REQUEST['size']!=''){
	 	$matrixPointSize = $_REQUEST['size'];	
	 }else{
	 	$matrixPointSize = 4;
	 }
    
	    $target_path = "uploads/";  
		$target_path = $target_path.time();   
		
		 $frm_link	=	 SITE_URL.'listing.php?file_id='.$file_id;
		
		// After getting all the data, now pass all the value to generate QR code.
		QRcode::png($frm_link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

		$date=date("Y-m-d H:i:s"); 
		$main_file_id=$file_id;
		$upload_by=$_SESSION['sess_user_id'];
		$qr_code_no = $_REQUEST['qr_code_no'];
		$quality = $_REQUEST['quality'];
		$size = $_REQUEST['size'];
		// print_r($code);
		// $QRcode=$code[1];
		 $sql = "INSERT INTO `tbl_qrcode`(`qr_code_no`,`quality`,`size`,`main_file_id`,`code`,`date`,`upload_by`) VALUES ('$qr_code_no','$quality','$size','$main_file_id','$time','$date','$upload_by')";
		if (mysqli_query($conn, $sql)) {
			header("Location: ".SITE_URL."QRlist.php");
			exit();
		}
		else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
}
?>
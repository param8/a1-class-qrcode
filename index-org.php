<?php 
include('library/php_qr_code/qrlib.php'); // Include a library for PHP QR code
include('connection.php');

if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){

	//its a location where generated QR code can be stored.
	$qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
	$set_qr_code_path = 'qr_assets/';

	// If directory is not created, the create a new directory 
	if(!file_exists($qr_code_file_path)){
    	mkdir($qr_code_file_path);
	}
	
	//Set a file name of each generated QR code
	$time=time();
	$filename	=	$qr_code_file_path.$time;
	
/* All the user generated data must be sanitize before the processing */
 if (isset($_REQUEST['level']) && $_REQUEST['level']!='')
    $errorCorrectionLevel = $_REQUEST['level'];

 if (isset($_REQUEST['size']) && $_REQUEST['size']!='')
    $matrixPointSize = $_REQUEST['size'];
	
	
	
	    $target_path = "uploads/";  
		$target_path = $target_path.time();   
		
		if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_path)) {  
		//	echo "File uploaded successfully!";  
		} else{  
			//echo "Sorry, file not uploaded, please try again!";  
		} 
	$frm_link	=	$target_path;
	
	// After getting all the data, now pass all the value to generate QR code.
	QRcode::png($frm_link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

		$date=date("Y-m-d"); 
		// $code=explode('/',$filename);
		// print_r($code);
		// $QRcode=$code[1];
		$sql = "INSERT INTO `tbl_qrcode`(`code`, `date`) VALUES ('$time','$date')";
		if (mysqli_query($conn, $sql)) {
		//	echo'Inserted';
		}
		else {
		//	echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
}
?>
<style>
    header{background: #f7f7f7;
    border-bottom: 2px solid #ff4300;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #ff4300;
    border-color: #ff4300;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #f7f7f7;
    color: #ff4300;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
    .forminput input{padding: 17px;
    height: 22px;
    padding-bottom: 43px;}
    
    .forminput h1{}
    
    .forminput  select{padding-left: 10px;
    height: 56px;}
    
    .forminput .btn-danger{
        margin-top: 0px;
    padding: 15px 35px 33px;
    color: white;
    background: #ff4300;
    border-color: #ff4300;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #f7f7f7;
    color: #ff4300;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #f7f7f7;
    border-top: 2px solid #ff4300;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}
    
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div>
    
    <header>
        <div class="container">
            
            <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="logo">
            <img src="https://webcadenceindia.com/images/brand/logo-dark.png">
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="listitem">
                <a class="btn btn-danger" href="https://demos.iparable.co.in/QRcode/QRlist.php" >List</a>
            </div>
        </div>
        </div>
        </div>
    </header>


	<div class="container ">
	 		<div class="row justify-content-md-center">
	 	
		<div class="col-md-10 col-sm-12 forminput">
		    <div class="row">
		        <div class="col-md-5 col-sm-12 headingh1">
		             <h1>QR Code Generate</h1>	    
		            <img src="default-preview-qr.svg">
		          </div>
		        	<div class="col-md-7 col-sm-12" style="padding:30px;background:#0b2f4b;box-shadow: 7px 0px 0px #06253e;">
		        	    
		        	    <?php if(isset($frm_link) and $frm_link!=""){?>
			<div class="alert alert-success">QR created for <strong>[<?php echo $frm_link;?>]</strong></div>
			<div class="text-center"><a  href="<?php echo $set_qr_code_path.basename($filename); ?>" download><img src="<?php echo $set_qr_code_path.basename($filename); ?>" /></a></div>
			<div style="margin-left: 41%;"><a class="btn btn-danger" href="<?php echo $set_qr_code_path.basename($filename); ?>" download>Download</a></div>
			<?php } ?>
			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Enter QR parameter</label>
					<input type="file" name="fileToUpload" id="fileToUpload" class="form-control" placeholder="Enter QR parameter" required>
				</div>
				<div class="form-group">
				<label>QR Code Level</label/>
					<select name="level" class="form-control">
						<option value="L">L - smallest</option>
						<option value="M">M</option>
						<option value="Q">Q</option>
						<option value="H" selected>H - best</option>
					</select>
				</div>
				<div class="form-group">
					<label>QR Code Size</label>
					<select name="size" class="form-control">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4" selected>4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Upload" class="btn btn-danger">
				</div>
			</form>
		        	    
		        	</div>
		        
		        
		    </div>
		    
			
		</div>
		</div>
	</div>
	
<div class="footer">
    
    <a href="">Design By :: I Parable</a>
    
</div>
</div>

	
	<!--Only these JS files are necessary--> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>   
</body>
</html>
<?php 
include('header.php');
include('library/php_qr_code/qrlib.php'); // Include a library for PHP QR code


if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
	// print_r($_REQUEST['code']);die;
	 //its a location where generated QR code can be stored.
	 $qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
	 $set_qr_code_path = 'qr_assets/';
 
	 // If directory is not created, the create a new directory 
	 if(!file_exists($qr_code_file_path)){
		 mkdir($qr_code_file_path);
	 }
	 
	 //Set a file name of each generated QR code
	 $time=$_REQUEST['code'];
	 $filename	=	$qr_code_file_path.$time;
	 
 /* All the user generated data must be sanitize before the processing */
  if (isset($_REQUEST['level']) && $_REQUEST['level']!='')
	 $errorCorrectionLevel = $_REQUEST['level'];
 
  if (isset($_REQUEST['size']) && $_REQUEST['size']!='')
	 $matrixPointSize = $_REQUEST['size'];
	 
	 
	 
		 $target_path = "uploads/";  
		 $target_path = $target_path.$_REQUEST['code'];   
		 
		 if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_path)) {  
		 //	echo "File uploaded successfully!";  
		 } else{  
		 //	echo "Sorry, file not uploaded, please try again!";  
		 } 
	 $frm_link	=	$target_path;
	 
	 // After getting all the data, now pass all the value to generate QR code.
	 //QRcode::png($frm_link, $filename, $errorCorrectionLevel, $matrixPointSize,2);
 
		 $date=date("Y-m-d"); 
		  $details=$_REQUEST['details'];
		 // print_r($code);
		 // $QRcode=$code[1];
		 
		 $sql = "UPDATE `tbl_qrcode` SET `details`='$details',`date`='$date' WHERE code='".$_REQUEST['code']."'";
		 if (mysqli_query($conn, $sql)) {
		 	//echo'Inserted';
		 }
		 else {
		 //	echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		 }
 }
 ?>

<style>
    header{background: #ffffff;
    border-bottom: 2px solid #9e1d18;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #ffffff;
    color: #9e1d18;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
   .forminput input {
    padding: 25px 22px 25px;
    height: 22px;
}
    
    .forminput h1{}
    
   .forminput select {
    padding-left: 10px;
    height: 52px;
}
    
    .forminput .btn-danger{
        margin-top: 0px;
/*    padding: 15px 35px 33px;*/
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #ffffff;
    color: #9e1d18;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #ffffff;
    border-top: 2px solid #9e1d18;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}

.name_file, .add-btn{
	padding-top: 13px!important;
    padding-bottom: 38px!important;
}  
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div>
    
<?php 
$fileid=$_GET['id'];
$sql="select * FROM `tbl_files` where file_id='$fileid'";
$res=mysqli_query($conn,$sql);
?>

	<div class="container ">
 		<div class="row justify-content-md-center">
	 	
		<div class="col-md-10 col-sm-12 forminput">
		    <div class="row">
			      <div class="col-md-12" style="padding:30px;background:#0b2f4b;box-shadow: 7px 0px 0px #06253e;">
			        <h1 style="color: ffffff; text-align: center; font-size: 28px;">Edit - QR Code Generate</h1>
			        <div class="form-group">
			          <form action="update.php" method="post" enctype="multipart/form-data" name="add_name" id="add_name">
			            <table class="table table-bordered table-hover" id="dynamic_field">
			            	<?php
			            	if(mysqli_num_rows($res) > 0)
			            	{
			            		while ($rows = mysqli_fetch_assoc($res)){
			            	?>
		            		<tr>
				                <td>
				                	<input type="hidden" name="edit_ids[]" value="<?=$rows['id'];?>">
				                	<input type="text" name="titles[]" placeholder="Enter Title" class="form-control name_title" value="<?=$rows['title'];?>" required /></td>
				                <td><input type="file" name="myfile[]" placeholder="Choose file" class="form-control name_file" /></td>
				            </tr>

			            	<?php
			            	 }	
			            	}
			            	?>
			            </table>
			            <input type="submit" class="btn btn-success add-btn" name="update" id="submit" value="Update">
			          </form>
			        </div>
			      </div>
			</div>
		    
		</div>
		</div>
	</div>

<div class="footer">
    
   <a href="http://webcadenceindia.com/" target="_blank">Design By :: Web Cadence</a>
    
</div>
</div>

	
	<!--Only these JS files are necessary--> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>   
</body>
</html>
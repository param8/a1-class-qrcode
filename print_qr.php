<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>GHE Australia Tour</title>
  <!-- favicons Icons -->
  <link rel="apple-touch-icon" sizes="180x180" href="https://austour.ghe-jcbtraining.com/images/logo.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="https://austour.ghe-jcbtraining.com/images/logo.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="https://austour.ghe-jcbtraining.com/images/logo.png" />
  <link rel="manifest" href="assets/images/favicons/site.webmanifest" />
  <meta name="description" content="" />

  <!-- fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link href="https://fonts.googleapis.com/css2?family=DM+Sans:opsz,wght@9..40,400;9..40,500;9..40,700&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Covered+By+Your+Grace&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Urbanist:ital,wght@0,400;0,700;0,800;1,800&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Manrope&amp;display=swap" rel="stylesheet">


<link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/vendors/bootstrap-select/bootstrap-select.min.css" />
<link rel="stylesheet" href="assets/vendors/animate/animate.min.css" />
<link rel="stylesheet" href="assets/vendors/fontawesome/css/all.min.css" />
<link rel="stylesheet" href="assets/vendors/jquery-ui/jquery-ui.css" />
<link rel="stylesheet" href="assets/vendors/jarallax/jarallax.css" />
<link rel="stylesheet" href="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.css" />
<link rel="stylesheet" href="assets/vendors/nouislider/nouislider.min.css" />
<link rel="stylesheet" href="assets/vendors/nouislider/nouislider.pips.css" />
<link rel="stylesheet" href="assets/vendors/tiny-slider/tiny-slider.css" />
<link rel="stylesheet" href="assets/vendors/trevlo-icons/style.css" />
<link rel="stylesheet" href="assets/vendors/daterangepicker-master/daterangepicker.css" />
<link rel="stylesheet" href="assets/vendors/owl-carousel/css/owl.carousel.min.css" />
<link rel="stylesheet" href="assets/vendors/owl-carousel/css/owl.theme.default.min.css" />

<!-- template styles -->
<link rel="stylesheet" href="assets/css/trevlo.css" />
</head>

<body class="custom-cursor">

  <div class="custom-cursor__cursor"></div>
  <div class="custom-cursor__cursor-two"></div>

  <!-- /.preloader -->
  <div class="page-wrapper">

<header class="main-header sticky-header sticky-header--one-page">
    <div class="container">
        <div class="main-header__inner">
            <div class="main-header__left">
                <div class="main-header__logo">
                    <a href="index.html">
                        <img src="images/logo.png" alt="GHE Ourania" width="146">
                    </a>
                </div><!-- /.main-header__logo -->
                <nav class="main-header__nav main-menu">
                    <ul class="main-menu__list one-page-scroll-menu">
                <li class="scrollToLink"><a href="#about">About</a></li>
                <li class="scrollToLink"><a href="#Hightlight">Hightlight</a></li>
                <li class="scrollToLink"><a href="#gallery">Gallery</a></li>
                <li class="scrollToLink"><a href="#contact">Contact</a></li>
</ul>
                </nav><!-- /.main-header__nav -->
            </div><!-- /.main-header__left -->
            
            <div class="main-header__right">
                <div class="mobile-nav__btn mobile-nav__toggler">
                    <span></span>
                    <span></span>
                    <span></span>
                </div><!-- /.mobile-nav__toggler -->               
                <div class="main-header__right-right">
                    <div class="main-header__phone">
                        <div class="main-header__phone-icon">
                            <span class="icon-phone-1"></span>
                        </div>
                        <div class="main-header__phone-text">
                            <p class="main-header__phone-title">Call Us</p>
                            <h4 class="main-header__phone-number"><a href="tel:+917973118638">(+91) 7973-118-638</a></h4>
                        </div>
                    </div><!-- /.main-header__phone -->
                </div><!-- /.main-header__right-right -->
            </div><!-- /.main-header__right -->
        </div><!-- /.main-header__inner -->
    </div><!-- /.container-fluid -->
</header><!-- /.main-header -->
<!-- main-slider-start -->
<section class="main-slider-one" id="home">
	<div class="main-slider-one__carousel trevlo-owl__carousel owl-carousel owl-theme"
		data-owl-options='{
		"items": 1,
		"margin": 0,
		"loop": true,
		"smartSpeed": 800,
		"animateOut": "fadeOut",
		"autoplayTimeout": 7000, 
		"nav": true,
		"navText": ["<span class=\"icon-left-arrow\"></span>","<span class=\"icon-right-arrow\"></span>"],
		"dots": false,
		"autoplay": false
		}'>
		<div class="item">
			<div class="main-slider-one__image" style="background-image: url(assets/images/backgrounds/slider-1-1.jpg);"></div>
            <div class="container">
                <div class="main-slider-one__content">
                    <h5 class="main-slider-one__sub-title">ESCORTED  Students Tour to Australia <img src="assets/images/shapes/slider-1-shape-1.png" alt="GHE Ourania"></h5>
                    <h3 class="main-slider-one__title">10 Days Tour <span>April - May</span> 
                    <span style="color:#ffad00;text-shadow:2px 3px 1px #ffffff;">AGE 6 TO 16 YEARS</span>
                     <img src="assets/images/shapes/slider-1-shape-2.png" alt="GHE Ourania"></h3>
                </div>
            </div>
		</div>
		
	</div><!-- banner-slider -->
  
</section>
<!-- main-slider-end -->

<!-- Tour Listing two Start -->
 <section class="tour-listing-two tour-listing section-space" id="Hightlight"> 
            <div class="container">
            <div class="sec-title text-center">
                        <!--<p class="sec-title__tagline">AGE 6 TO 16 years</p>-->
                        <h2 class="sec-title__title">10 DAYS TOUR WILL COVER</h2>
                    </div>
                <div class="row">
                    <div class="col-xl-6 col-md-6 wow animated fadeInUp" data-wow-delay="0.1s" data-wow-duration="1500ms">
                        <div class="tour-listing-two__card tour-listing__card">
                            <a href="#form" class="tour-listing__card-image-box">
                                <img src="images/service/1.png" class="tour-listing__card-image">                                
                                <div class="tour-listing__card-image-overlay"></div><!-- /.tour-listing__card-image-overlay -->
                            </a><!-- /.tour-listing__card-image-box -->                            
                            <div class="tour-listing-two__card-show-content">
                                <div class="tour-listing-two__card-show-title-box">
                                    <h3 class="tour-listing-two__card-show-title tour-listing__card-title">
                                     <a href="#form"> Culture</a></h3>
                                </div><!-- /.tour-listing-two__card-show-title-box -->
                                <!-- /.tour-listing__card-bottom -->
                            </div><!-- /.tour-listing__card-show-content -->                           
                        </div><!-- /.tour-listing__card -->
                    </div><!-- /.col-xl-4 col-md-6 -->
                    <!-- /.col-xl-4 col-md-6 -->
                   <div class="col-xl-6 col-md-6 wow animated fadeInUp" data-wow-delay="0.1s" data-wow-duration="1500ms">
                        <div class="tour-listing-two__card tour-listing__card">
                            <a href="#form" class="tour-listing__card-image-box">
                                <img src="images/service/2.jpg" class="tour-listing__card-image">                                
                                <div class="tour-listing__card-image-overlay"></div><!-- /.tour-listing__card-image-overlay -->
                            </a><!-- /.tour-listing__card-image-box -->                            
                            <div class="tour-listing-two__card-show-content">
                                <div class="tour-listing-two__card-show-title-box">
                                    <h3 class="tour-listing-two__card-show-title tour-listing__card-title">
                                     <a href="#form">Education</a></h3>
                                </div><!-- /.tour-listing-two__card-show-title-box -->
                                <!-- /.tour-listing__card-bottom -->
                            </div><!-- /.tour-listing__card-show-content -->
                          
                        </div><!-- /.tour-listing__card -->
                    </div><!-- /.col-xl-4 col-md-6 -->
                    <div class="col-xl-6 col-md-6 wow animated fadeInUp" data-wow-delay="0.1s" data-wow-duration="1500ms">
                        <div class="tour-listing-two__card tour-listing__card">
                            <a href="#form" class="tour-listing__card-image-box">
                                <img src="images/service/3.jpg" class="tour-listing__card-image">                                
                                <div class="tour-listing__card-image-overlay"></div><!-- /.tour-listing__card-image-overlay -->
                            </a><!-- /.tour-listing__card-image-box -->                            
                            <div class="tour-listing-two__card-show-content">
                                <div class="tour-listing-two__card-show-title-box">
                                    <h3 class="tour-listing-two__card-show-title tour-listing__card-title">
                                     <a href="#form">Environment</a></h3>
                                </div><!-- /.tour-listing-two__card-show-title-box -->
                                <!-- /.tour-listing__card-bottom -->
                            </div><!-- /.tour-listing__card-show-content -->
                           
                        </div><!-- /.tour-listing__card -->
                    </div><!-- /.col-xl-4 col-md-6 -->
                    
                    <div class="col-xl-6 col-md-6 wow animated fadeInUp" data-wow-delay="0.1s" data-wow-duration="1500ms">
                        <div class="tour-listing-two__card tour-listing__card">
                            <a href="#form" class="tour-listing__card-image-box">
                                <img src="images/service/4.jpg" class="tour-listing__card-image">                                
                                <div class="tour-listing__card-image-overlay"></div><!-- /.tour-listing__card-image-overlay -->
                            </a><!-- /.tour-listing__card-image-box -->                            
                            <div class="tour-listing-two__card-show-content">
                                <div class="tour-listing-two__card-show-title-box">
                                    <h3 class="tour-listing-two__card-show-title tour-listing__card-title">
                                     <a href="#form">Skilling</a></h3>
                                </div><!-- /.tour-listing-two__card-show-title-box -->
                                <!-- /.tour-listing__card-bottom -->
                            </div><!-- /.tour-listing__card-show-content -->
                           
                        </div><!-- /.tour-listing__card -->
                    </div><!-- /.col-xl-4 col-md-6 -->
                    
                    
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <!-- Tour Listing Page One End -->
        <!-- Why Choose Two Start -->
<section class="why-choose-one">
    <div class="container">
       <div class="row align-items-center">
        <div class="col-lg-12 col-xl-12 wow fadeInLeft" data-wow-delay="200ms">        
             <div class="why-choose-two__content">
              <h5 class="text-white">
                   Best option for Students who want to <br>Study & Settle Abroad
              </h5>
             
            </div><!-- /.why-choose-two__content -->
        </div><!-- /.col-lg-6 col-xl-6 -->

       </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!-- Why Choose Two End -->
<!-- Why Choose Two End -->



<!-- Destination Start -->
<section class="destination-three" id="gallery">
    <div class="container"> 
   
        <div class="row">
            <div class="col-xl-4 col-md-6  wow fadeInUp" data-wow-delay="100ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/1.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                        
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-3 col-md-6 wow fadeInUp" data-wow-delay="150ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/13.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                       
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-5 col-md-6 wow fadeInUp" data-wow-delay="200ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/3.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                       
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-5 col-md-6 wow fadeInUp" data-wow-delay="250ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/4.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                     
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-4 col-md-6  wow fadeInUp" data-wow-delay="300ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/5.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                      
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-3 col-md-6  wow fadeInUp" data-wow-delay="350ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/6.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                      
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-4 col-md-6  wow fadeInUp" data-wow-delay="100ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/16.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                        
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-3 col-md-6 wow fadeInUp" data-wow-delay="150ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/8.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                       
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-5 col-md-6 wow fadeInUp" data-wow-delay="200ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/9.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                       
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-5 col-md-6 wow fadeInUp" data-wow-delay="250ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/10.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                     
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-4 col-md-6  wow fadeInUp" data-wow-delay="300ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/11.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                      
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
            <div class="col-xl-3 col-md-6  wow fadeInUp" data-wow-delay="350ms">
                <div class="destination-one__card">
                    <div class="destination-one__card-img-box destination-one__card-img-box--round">
                        <img src="images/gallery/12.jpg" alt="destination" class="destination-one__card-img destination-one__card-img--round">                      
                    </div><!-- /.destination-one__card-img-box -->
                </div><!-- /.destination-one__card -->
            </div>
        </div>
    </div>
</section>

<section class="contact-page section-space-top" id="contact">
    <div class="container">
            <div class="sec-title text-center">                                
                <h2 class="sec-title__title">Application Form</h2><!-- /.sec-title__title -->
            </div><!-- /.sec-title -->
							<!-- REGISTER FORM -->
							
		
            <div class="form wow fadeInUp" data-wow-delay="100ms">						
        <form id="register-form" action="thank-you.php" method="post" class="contact-page__form form-one row gutter-20 contact-form-validated">
            <!-- Form Input -->
            <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
            <div class="form-one__group">
                <p>NAME OF APPLICANT *</p>
                <input type="text" name="sname" id="sname" class="form-control form-one__input form-one__input name" placeholder="Enter Applicant Name*" required=""> 
            </div>
            </div>
             <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
             <div class="form-one__group">
                <p>DATE OF BIRTH *</p>
                <input type="Date" name="dob" id="dob" class="form-control form-one__input name" required=""> 
            </div>
            </div>
            <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
            <div class="form-one__group">
                <p>E-MAIL*</p>
                <input type="email" name="email" id="email" class="form-control form-one__input email" placeholder="Enter Your Email*" required=""> 
            </div>
            </div>
            
            <!-- Form Input -->       
            
            <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
            <div class="form-one__group">
                <p>TELEPHONE NUMBER*</p>
                <input type="tel" name="phone" class="form-control form-one__input phone" placeholder="Enter Your  Phone Number*" required=""> 
            </div>
            </div>
              <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
              <div class="form-one__group">
                <p>PASSPORT NUMBER  *</p>
                <input type="text" name="pnum" id="pnum" class="form-control form-one__input name" placeholder="Enter Passport no.*" required=""> 
            </div>
            </div>
            
             
            <!-- Form Input -->
             <div class="col-md-4 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
            <div class="form-one__group">
                <p>EDUCATION QUALIFICATION  *</p>
                <input type="text" name="Education" id="Education" class="form-control form-one__input name" placeholder="Enter Your Qualification*" required=""> 
            </div>
            </div>
              <div class="col-md-12 wow animated fadeInUp"  data-wow-delay="0s" data-wow-duration="1500ms">
              <div class="form-one__group">
                <p>COMPLETE POSTAL ADDRESS *</p>
                <textarea name="address" id="address" class="form-control form-one__input email" placeholder="Enter Your Address*" required> </textarea>
            </div>
            </div>
                 
           
           
           
            <!-- Form Button -->
            <div class="col-md-12 form-btn">  
            <div class="form-one__group">
                <button type="submit" id="submtbtn" name="submtbtn" value="submit" class="trevlo-btn--base">Submit </button> 
            </div>
            </div>
                                                          
                             
        </form>
        </div>
    </div>
			</section>
 
<!-- CTA Two End -->
<footer class="main-footer @@extraClassName">	
	<!-- /.main-footer__bg -->
	<div class="main-footer__bottom">
		<div class="container">
			<div class="main-footer__bottom-inner">
				<p class="main-footer__copyright">
					VPO khudda 60 kms Jullundhar Pathankot Highway District Hosiarpur Punjab 144305<br>
                    <strong><strong>(+91) 7973-118-638 | info@ghe-jcbtraining.com</strong></strong>
				</p>
                <p class="main-footer__copyright">
					Copyright <strong>GHE Australia Tour</strong> | All Right Reserved | Design By <strong>Web Cadence</strong>
				</p>
			</div><!-- /.main-footer__inner -->
		</div><!-- /.container -->
	</div><!-- /.main-footer__bottom -->
</footer><!-- /.main-footer -->

</div><!-- /.page-wrapper -->




<a href="#" data-target="html" class="scroll-to-target scroll-to-top">
    <span class="scroll-to-top__text">back top</span>
    <span class="scroll-to-top__wrapper"><span class="scroll-to-top__inner"></span></span>
</a>

<script src="assets/vendors/jquery/jquery-3.7.0.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/jarallax/jarallax.min.js"></script>
<script src="assets/vendors/jquery-ui/jquery-ui.js"></script>
<script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
<script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
<script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
<script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="assets/vendors/nouislider/nouislider.min.js"></script>
<script src="assets/vendors/tiny-slider/tiny-slider.js"></script>
<script src="assets/vendors/wnumb/wNumb.min.js"></script>
<script src="assets/vendors/owl-carousel/js/owl.carousel.min.js"></script>
<script src="assets/vendors/wow/wow.js"></script>
<script src="assets/vendors/tilt/tilt.jquery.js"></script>
<script src="assets/vendors/simpleParallax/simpleParallax.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.min.js"></script>
<script src="assets/vendors/isotope/isotope.js"></script>
<script src="assets/vendors/countdown/countdown.min.js"></script>
<script src="assets/vendors/daterangepicker-master/moment.min.js"></script>
<script src="assets/vendors/daterangepicker-master/daterangepicker.js"></script>
<script src="assets/vendors/jquery-circleType/jquery.circleType.js"></script>
<script src="assets/vendors/jquery-lettering/jquery.lettering.min.js"></script>
<!-- template js -->
<script src="assets/js/trevlo.js"></script>
</body>
</html>
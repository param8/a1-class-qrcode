<?php
ob_start();
session_start();
?>
<?php 
include('header.php');
include('library/php_qr_code/qrlib.php'); // Include a library for PHP QR code


if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){

	//its a location where generated QR code can be stored.
	$qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
	$set_qr_code_path = 'qr_assets/';

	// If directory is not created, the create a new directory 
	if(!file_exists($qr_code_file_path)){
    	mkdir($qr_code_file_path);
	}
	
	//Set a file name of each generated QR code
	$time=time();
	$filename	=	$qr_code_file_path.$time.'.png';
	
/* All the user generated data must be sanitize before the processing */
 if (isset($_REQUEST['level']) && $_REQUEST['level']!='')
    $errorCorrectionLevel = $_REQUEST['level'];

 if (isset($_REQUEST['size']) && $_REQUEST['size']!='')
    $matrixPointSize = $_REQUEST['size'];
	
	
	
	    $target_path = "uploads/";  
		$target_path = $target_path.time();   
		
		if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_path)) {  
		//	echo "File uploaded successfully!";  
		} else{  
			//echo "Sorry, file not uploaded, please try again!";  
		} $frm_link	=	 SITE_URL.'download.php/'.$target_path;
	//$frm_link	=	 SITE_URL.$target_path;
	
	// After getting all the data, now pass all the value to generate QR code.
	QRcode::png($frm_link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

		$date=date("Y-m-d"); 
		$details=$_POST['details'];
		$upload_by=$_SESSION['sess_user_id'];
		// print_r($code);
		// $QRcode=$code[1];
		$sql = "INSERT INTO `tbl_qrcode`(`code`,`details`, `date`,`upload_by`) VALUES ('$time','$details','$date','$upload_by')";
		if (mysqli_query($conn, $sql)) {
		//	echo'Inserted';
		}
		else {
		//	echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
}
?>
<style>
    header{background: #ffffff;
    border-bottom: 2px solid #9e1d18;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #ffffff;
    color: #9e1d18;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
   .forminput input {
    padding: 25px 22px 25px;
    height: 22px;
}
    
    .forminput h1{}
    
   .forminput select {
    padding-left: 10px;
    height: 52px;
}
    
    .forminput .btn-danger{
        margin-top: 0px;
/*    padding: 15px 35px 33px;*/
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #ffffff;
    color: #9e1d18;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #ffffff;
    border-top: 2px solid #9e1d18;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}

.name_file, .add-btn{
	padding-top: 13px!important;
    padding-bottom: 38px!important;
}
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div>
    
	<div class="container ">
	 		<div class="row justify-content-md-center">
	 	
		<div class="col-md-10 col-sm-12 forminput">
		    <div class="row">
			      <div class="col-md-12" style="padding:30px;background:#0b2f4b;box-shadow: 7px 0px 0px #06253e;">
			      	<h1 style="color: ffffff; text-align: center; font-size: 28px;">QR Code Generate</h1>
			        <div class="form-group">
			          <form action="action.php" method="post" enctype="multipart/form-data" name="add_name" id="add_name">
			              <div class="row">
			                  <div class="col-sm-12 col-md-6 col-lg-6 ">
			                      <div class="form-group">
                                    <label>QR Code No. <span class="text-danger">*</span></label>
			                      <input type="text" name="qr_code_no" placeholder="QR Code No." class="form-control name_title" required />
			                      </div>
			                  </div>
			                  <div class="col-sm-12 col-md-6 col-lg-6 ">
			                       <div class="form-group">
                                    <label>Quality</label>
			                      <input type="text" name="quality" placeholder="Enter Quality" class="form-control name_title"  />
			                      </div>
			                  </div>
			                  <div class="col-sm-12 col-md-6 col-lg-6 ">
			                       <div class="form-group">
                                    <label>Size</label>
			                      <input type="text" name="size" placeholder="Enter Size" class="form-control name_title"  />
			                      </div>
			                  </div>
			                  
			              </div>
			            <table class="table table-bordered table-hover" id="dynamic_field">
			              <tr>
			                <td><input type="text" name="titles[]" placeholder="Enter Title" class="form-control name_title" required /></td>
			                <td><input type="file" accept=".pdf" name="myfile[]" placeholder="Choose file" class="form-control name_file" required /></td>
			                <td><button type="button" name="add" id="add" class="btn btn-primary">Add More</button></td>  
			              </tr>
			            </table>
			            <input type="submit" class="btn btn-success add-btn" name="submit" id="submit" value="Submit">
			          </form>
			        </div>
			      </div>
			</div>
		    
		</div>
		</div>
	</div>
	
<div class="footer">
    
  <a href="http://webcadenceindia.com/" target="_blank">Design By :: Web Cadence</a>
    
</div>
</div>

	
	<!--Only these JS files are necessary--> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>   
</body>
</html>
<script>
$(document).ready(function(){
   
  var i = 1;
	var length;
	//var addamount = 0;
   var addamount = 700;

  $("#add").click(function(){
    
	 <!-- var rowIndex = $('#dynamic_field').find('tr').length;	 -->
	 <!-- console.log('rowIndex: ' + rowIndex); -->
	 <!-- console.log('amount: ' + addamount); -->
	 <!-- var currentAmont = rowIndex * 700; -->
	 <!-- console.log('current amount: ' + currentAmont); -->
	 <!-- addamount += currentAmont; -->
	 
	 addamount += 700;
	 console.log('amount: ' + addamount);
   i++;
      $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="titles[]" placeholder="Enter title" class="form-control name_title" required /></td><td><input type="file" name="myfile[]" accept=".pdf" placeholder="Enter your Email" class="form-control name_file" required /></td> <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
    });

  $(document).on('click', '.btn_remove', function(){  
	addamount -= 700;
	console.log('amount: ' + addamount);
	
	<!-- var rowIndex = $('#dynamic_field').find('tr').length;	 -->
	 <!-- addamount -= (700 * rowIndex); -->
	 <!-- console.log(addamount); -->
	 
	  var button_id = $(this).attr("id");     
      $('#row'+button_id+'').remove();  
    });
	


    // $("#submit").on('click',function(event){
    // var file_data = $('#sortpicture').prop('files')[0];   
    // var formdata = $("#add_name").serialize();
    // formdata.append('file', file_data);
	//   console.log(formdata);
	  
	//   event.preventDefault()
      
    //   $.ajax({
    //     url   :"action.php",
    //     type  :"POST",
    //     data  :formdata,
    //     cache :false,
    //     success:function(result){
    //       alert(result);
    //       $("#add_name")[0].reset();
    //     }
    //   });
      
    // });
  });
</script>
-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 03, 2024 at 03:33 AM
-- Server version: 8.0.36
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `globalpath_a1_class`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_count`
--

CREATE TABLE `tbl_count` (
  `id` int NOT NULL,
  `code` int NOT NULL,
  `count` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_files`
--

CREATE TABLE `tbl_files` (
  `id` int NOT NULL,
  `file_id` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `filename` varchar(100) NOT NULL,
  `user_file` varchar(100) NOT NULL,
  `added_date` datetime NOT NULL,
  `added_by` int NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_files`
--

INSERT INTO `tbl_files` (`id`, `file_id`, `title`, `filename`, `user_file`, `added_date`, `added_by`, `modified_date`, `modified_by`) VALUES
(1, '660a8251cd957', 'Test', './uploadFiles/1711964753883911936660a8251cd96b.pdf', 'bill.pdf', '2024-04-01 09:45:53', 1, NULL, NULL),
(2, '660a8251cd957', 'Test 2', './uploadFiles/1711964753587942254660a8251d2939.pdf', 'PARAM JAN.pdf', '2024-04-01 09:45:53', 1, NULL, NULL),
(3, '660a99117887d', 'Michel Angelo Collection', './uploadFiles/17121330202025802060660d139c64f1f.pdf', 'WhatsApp Image 2024-04-03 at 11.27.30 AM.pdf', '2024-04-01 11:22:57', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_qrcode`
--

CREATE TABLE `tbl_qrcode` (
  `id` int NOT NULL,
  `qr_code_no` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `quality` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `size` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `main_file_id` varchar(50) NOT NULL,
  `code` varchar(250) NOT NULL,
  `details` varchar(250) DEFAULT NULL,
  `counter` int DEFAULT NULL,
  `date` datetime NOT NULL,
  `upload_by` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_qrcode`
--

INSERT INTO `tbl_qrcode` (`id`, `qr_code_no`, `quality`, `size`, `main_file_id`, `code`, `details`, `counter`, `date`, `upload_by`) VALUES
(1, NULL, NULL, NULL, '660a8251cd957', '1711964753', NULL, NULL, '2024-04-01 09:45:53', 1),
(2, '1190/27/13E/D55/-MB-Sl012907', 'DIOR PEARL', '108 X 64 20MM', '660a99117887d', '1711970577', NULL, 15, '2024-04-01 11:22:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int NOT NULL,
  `name` varchar(250) NOT NULL,
  `contact` bigint NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `contact`, `email`, `password`, `date`) VALUES
(1, 'Admin', 1234567893, 'admin@admin.com', 'admin@1234', '2020-08-09 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_count`
--
ALTER TABLE `tbl_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_files`
--
ALTER TABLE `tbl_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_qrcode`
--
ALTER TABLE `tbl_qrcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_count`
--
ALTER TABLE `tbl_count`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_files`
--
ALTER TABLE `tbl_files`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_qrcode`
--
ALTER TABLE `tbl_qrcode`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include('header.php');?>
<!DOCTYPE html>
<html lang="en">
<style>
    header{background: #ffffff;
    border-bottom: 2px solid #9e1d18;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #ffffff;
    color: #9e1d18;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
   .forminput input {
    padding: 25px 22px 25px;
    height: 22px;
}
    
    .forminput h1{}
    
   .forminput select {
    padding-left: 10px;
    height: 52px;
}
    
    .forminput .btn-danger{
        margin-top: 0px;
    padding: 15px 35px 33px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #ffffff;
    color: #9e1d18;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #ffffff;
    border-top: 2px solid #9e1d18;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}
    
</style>
<?php
if(isset($_REQUEST['Delete']) and $_REQUEST['Delete']!=""){
    //  print_r($_POST['delete_id']); 
      $sql = "DELETE FROM `tbl_qrcode` WHERE id='".$_POST['delete_id']."'";
		 if (mysqli_query($conn, $sql)) {
		   //echo '<p style="margin-left: 41%;">Data Deleted Successfully</p>';
		 }
		 else {
		 //	echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		 }
}

?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">           
  <table class="table">
    <thead>
      <tr>
        <th>S.no</th>
        <th>QR Code</th>
        <th>Details</th>
        <th class="text-center">Download Count</th>
        <th>Date</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
   <?php 
   
   $sno=1;
    $sql="SELECT * FROM `tbl_qrcode` where upload_by='".$_SESSION['sess_user_id']."' ORDER BY date desc";
	 $res=mysqli_query($conn,$sql);
	 while ($rows = mysqli_fetch_assoc($res)){
    $sql_files="SELECT * FROM `tbl_files` WHERE file_id='".$rows['main_file_id']."'";
    $res_files=mysqli_query($conn,$sql_files);
     $rowres_files=mysqli_num_rows($res_files);
   //  echo  $count=$res_count['num_rows'];?>
      <tr>
        <td><?php echo $sno; ?></td>
        <td><a href="<?=SITE_URL?>qr_assets/<?php echo $rows['code'].'.png'; ?>"><img src="qr_assets/<?php echo $rows['code'].'.png'; ?>" style="width: 75px;"></a><br>
            <a class="btn btn-xs btn-danger" style="margin-top: 6px; padding: 2px 12px 1px 12px; font-size: 12px;"  href="javascript:void(0)" onclick="print_qr(<?=$rows['id']?>)" >Download</a>
        </td>
        <td>
            <?php 
                if($rowres_files>0)
                {   $x=1;
                    while ($rows1 = mysqli_fetch_assoc($res_files)){
                        echo $x." - ".$rows1['title']."<br>";
                        $x++;
                    }
                }
             ?>
        </td>
        <td class="text-center"><?php echo ($rows['counter'])?$rows['counter']:0; ?></td>
        <td><?php echo date('Y-m-d', strtotime($rows['date'])); ?></td>
        <td> <a href="edit.php?id=<?php echo $rows['main_file_id'];?>" data-toggle="tooltip" class="btn btn-warning btn-sm" data-original-title="Edit"><i class="fa fa-pencil"></i>Edit</a>
         <!--<a href="javascript:void(0)" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#approveModal" onclick="myFunction('<?php echo $rows['id'];?>')">Delete</a>-->
         </td>
      </tr>
      <?php 
    $sno++;
    }?>
    </tbody>
  </table>
</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="approveModal">
           <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Delete</strong></h4>
              </div>
        
             <form action="" method="post">
                <div class="modal-body">
                  <div class="row"style="padding: 22px;" >
                   <strong>Do you really want to Delete?</strong><br><br>	 
                  <input type="hidden"  id="delete_id" name="delete_id" >
                </div>
                </div>
        
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <input type="submit" name="Delete" id="Delete" value="Delete" class="btn btn-danger"/>
                  <!--<button type="submit" value="Delete" class="btn btn-danger">Delete</button>-->
                </div>
              </form>
            </div><!-- /.modal-content -->
          </div>
  </div>
  
  
        <div class="modal fade" tabindex="-1" role="dialog" id="print_qr">
           <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Print QR</strong></h4>
              </div>
        
            
                <div class="modal-body"  id="form_qr">
                  
                </div>
        
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <a href="javascript:void(0)" onclick="print_qr_form()" class="btn btn-primary"/>Print</a>
                  <!--<button type="submit" value="Delete" class="btn btn-danger">Delete</button>-->
                </div>
             
            </div><!-- /.modal-content -->
          </div>
  </div>
	
<div class="footer">
    
  <a href="http://webcadenceindia.com/" target="_blank">Design By :: Web Cadence</a>
    
</div>

</body>
<script>
function myFunction(id) {
    // alert(id);
  document.getElementById("delete_id").value = id;
}

function print_qr(id){
    $.ajax({
    url: 'print_qr_form.php',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
        $('#print_qr').modal('show');
       $('#form_qr').html(response);
        //console.log(response)
    //   $('#servicesModel').modal('show');
    //   $('#viewServices_detail').html(response);
    //   togglePay();
    }
  });
}

function print_qr_form() {
    var printContents = document.getElementById('form_qr').innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}
</script>
</html>
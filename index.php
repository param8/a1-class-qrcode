<?php include('login_header.php');?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div> 
   
   <?php 
include('library/php_qr_code/qrlib.php'); // Include a library for PHP QR code

if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
  //print_r($_POST); die;
	//its a location where generated QR code can be stored.
	$qr_code_file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'qr_assets'.DIRECTORY_SEPARATOR;
	$set_qr_code_path = 'qr_assets/';

	// If directory is not created, the create a new directory 
	if(!file_exists($qr_code_file_path)){
    	mkdir($qr_code_file_path);
	}
	
	//Set a file name of each generated QR code
	$time=time();
	$filename	=	$qr_code_file_path.$time.'.png';
	
/* All the user generated data must be sanitize before the processing */
 if (isset($_REQUEST['level']) && $_REQUEST['level']!='')
    $errorCorrectionLevel = $_REQUEST['level'];

 if (isset($_REQUEST['size']) && $_REQUEST['size']!='')
    $matrixPointSize = $_REQUEST['size'];
	
	
	
	    $target_path = "uploads/";  
		$target_path = $target_path.time();   
		
		if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_path)) {  
		//	echo "File uploaded successfully!";  
		} else{  
			//echo "Sorry, file not uploaded, please try again!";  
		} 
	$frm_link	=	'http://localhost/QRCODE_NEW/QRcode/'.$target_path;
	
	// After getting all the data, now pass all the value to generate QR code.
	QRcode::png($frm_link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

		$date=date("Y-m-d"); 
		$details=$_POST['details'];
		// print_r($code);
		// $QRcode=$code[1];
		$sql = "INSERT INTO `tbl_qrcode`(`code`,`details`, `date`) VALUES ('$time','$details','$date')";
		if (mysqli_query($conn, $sql)) {
		//	echo'Inserted';
		}
		else {
		//	echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
}
?>
<style>
    header{background: #ffffff;
    border-bottom: 2px solid #9e1d18;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #ffffff;
    color: #9e1d18;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
   .forminput input {
    padding: 25px 22px 25px;
    height: 22px;
}
    
    .forminput h1{}
    
   .forminput select {
    padding-left: 10px;
    height: 52px;
}
    
    .forminput .btn-danger{
        margin-top: 0px;
    padding: 15px 35px 33px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #ffffff;
    color: #9e1d18;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #ffffff;
    border-top: 2px solid #9e1d18;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}
    
</style>
	<div class="container ">
	 		<div class="row justify-content-md-center">
	 	
		<div class="col-md-10 col-sm-12 forminput">
		    <div class="row">
		        <div class="col-md-5 col-sm-12 headingh1">
		             <h1>QR Code Generate</h1>
		             	    <?php if(isset($frm_link) and $frm_link!=""){?>
			<a  href="<?php echo $set_qr_code_path.basename($filename); ?>" download><img src="<?php echo $set_qr_code_path.basename($filename); ?>" /></a>
			<a class="btn btn-danger" style="padding-bottom: 12px; margin-top: 13px;"  href="<?php echo $set_qr_code_path.basename($filename); ?>" download>Download</a>
			
			<?php }else{ ?>
		            <img src="default-preview-qr.svg"><?php }?>
		          </div>
			<div class="col-md-7 col-sm-12" style="padding:30px;background:#0b2f4b;box-shadow: 7px 0px 0px #06253e;">
		        	    
					<?php if(isset($frm_link) and $frm_link!=""){?><div class="alert alert-success">QR created for <strong>[<?php echo $frm_link;?>]</strong></div><?php } ?>
			
		        <form action="<?php echo SITE_URL;?>login.php" method="post">
				<div class="form-group">
					<label>Email Address </label>
					 <input type="email" name="username" class="form-control" required>
				</div>
                <div class="form-group">
					<label>Password </label>
					 <input type="password" name="password"  class="form-control" required>
				</div>
				
				<div class="form-group">
				<button type="submit" class="btn btn-danger" style="padding: 10px;">LOGIN</button>
				</div>
			</form>
		        	</div>
		        
		        
		    </div>
		    
			
		</div>
		</div>
	</div>
	
<div class="footer">
    
   <a href="http://webcadenceindia.com/" target="_blank">Design By :: Web Cadence</a>
    
</div>
</div>

	
	<!--Only these JS files are necessary--> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>   
</body>
</html>
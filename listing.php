<?php
ob_start();
session_start();
include('connection.php');
// $code=explode('/',$_SERVER['PHP_SELF']);
$fileid = $_REQUEST['file_id'];
    
    // update count
    $count_sql="SELECT * FROM `tbl_qrcode` where main_file_id = '$fileid'";
    $res_count_sql=mysqli_query($conn,$count_sql);
    if(mysqli_num_rows($res_count_sql) > 0)
    {
        while ($count_rows = mysqli_fetch_assoc($res_count_sql)){
            $current_count = $count_rows['counter'];
        }

        if($current_count){
            $current_count++;
        }else{
            $current_count=1;
        }

        $sql_update_counter = "UPDATE tbl_qrcode SET counter = $current_count WHERE main_file_id = '$fileid'";
        mysqli_query($conn, $sql_update_counter);   
    } 
    
$sql="SELECT * FROM `tbl_files` where file_id='$fileid'";
$res=mysqli_query($conn,$sql);
$rows = mysqli_num_rows($res);

?>
<!DOCTYPE html>
<html lang="en">
<style>
    header{background: #ffffff;
    border-bottom: 2px solid #9e1d18;}
     header .logo{}
      header .logo img{padding: 10px;
    width: 170px;}
      header .listitem{text-align: right;}
      header .listitem .btn-danger{margin-top: 20px;
    padding: 8px 35px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    header .listitem .btn-danger:hover {
    background: #ffffff;
    color: #9e1d18;
}
    .forminput{background: #ffffff;
    margin: 2% auto;
    border: 1px solid #e4e4e4;
    margin-top: 40px;
    margin-bottom: 40px;}
   .forminput input {
    padding: 25px 22px 25px;
    height: 22px;
}
    
    .forminput h1{}
    
   .forminput select {
    padding-left: 10px;
    height: 52px;
}
    
    .forminput .btn-danger{
        margin-top: 0px;
    padding: 15px 35px 33px;
    color: white;
    background: #9e1d18;
    border-color: #9e1d18;
    font-weight: 600;}
    
    .forminput  .btn-danger:hover{ background: #ffffff;
    color: #9e1d18;}
    
    .footer{
    text-align: center;
    padding: 10px 10px;
    background: #ffffff;
    border-top: 2px solid #9e1d18;
}
.footer a{color:#000;}

.headingh1 {
    text-align: center;
    background: #06253e;
    padding: 45px 10px 45px;
    box-shadow: -7px 0px 0px #0b2f4b;
}

.headingh1 h1 {
    text-align: center;
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 35px;
    text-transform: uppercase;
    color: #ffffff;
}
    
    .forminput label{
    font-size: 15px;
    font-weight: 500;
    color: #929292;}
    
    .headingh1 img {
    width: 250px;
}

.file-info{
    background-color: cadetblue;
    color: #fff;
    padding: 8px;
}  
</style>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
  <div class="row">
      <div class="col-md-10 col-md-offset-2" style="padding-top:60px;">
        <h4 class="text-center file-info">There are <?=$rows?> files in this QR Code</h3>
        <table class="table">
            <thead>
              <tr>
                <th>S.no</th>
                <th>Title</th>
                <th>Download File</th>
              </tr>
            </thead>
            <tbody>
           <?php $sno=1;
            if($rows > 0)
            {
                while ($rows = mysqli_fetch_assoc($res)){
                    
            ?>
                <tr>
                    <td><?php echo $sno; ?></td>
                    <td><?php echo $rows['title']; ?></td>
                    <td><a href="<?php echo $rows['filename']; ?>" target="_blank"><?php echo $rows['user_file']; ?></a></td>
                </tr>  
            <?php 
                $sno++;
                }
            }
            ?>
            </tbody>
          </table>
      </div>
  </div>           
</div>
 
</body>
</html>